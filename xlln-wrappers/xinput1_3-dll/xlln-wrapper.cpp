#include "../dllmain.hpp"
#include "xlln-wrapper.hpp"
#include "../utils/util-hook.hpp"
#include "../utils/utils.hpp"

BOOL InitXllnWrapper()
{

	return TRUE;
}

BOOL UninitXllnWrapper()
{

	return TRUE;
}

// #3
DWORD WINAPI XInputSetState(
	DWORD            dwUserIndex,
	XINPUT_VIBRATION *pVibration
)
{
	return ERROR_SUCCESS;
}

// #7
DWORD WINAPI XInputGetBatteryInformation(
	DWORD                      dwUserIndex,
	BYTE                       devType,
	XINPUT_BATTERY_INFORMATION *pBatteryInformation
)
{
	return ERROR_INVALID_PARAMETER;
}

// #8
DWORD WINAPI XInputGetKeystroke(
	DWORD             dwUserIndex,
	DWORD             dwReserved,
	PXINPUT_KEYSTROKE pKeystroke
)
{
	return ERROR_INVALID_PARAMETER;
}
