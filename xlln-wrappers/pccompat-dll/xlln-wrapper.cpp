#include "../dllmain.hpp"
#include "xlln-wrapper.hpp"
#include "../utils/util-hook.hpp"
#include "../utils/utils.hpp"

#ifdef _DEBUG
#define xtra2() __debugbreak();
#else
#define xtra2() ;
#endif
#define xtra() \
			   xtra2()

BOOL InitXllnWrapper()
{

	return TRUE;
}

BOOL UninitXllnWrapper()
{

	return TRUE;
}

// #1
PCC_EXPORT PCC_Evaluate()
{
	xtra();
	return S_OK;
}

// #2
PCC_EXPORT PCC_GetBooleanProperty(wchar_t* property_name, int *rtn_value)
{
	xtra();
	//MessageBoxW(0, property_name, L"PCC_GetBooleanProperty", MB_OK);
	if (!rtn_value) {
		return E_POINTER;
	}

	if (wcscmp(L"VideoDeviceSupportsHardwareTnL", property_name) == 0) {
		*rtn_value = 1;
	}
	else if (wcscmp(L"CinematicShadow", property_name) == 0) {
		*rtn_value = 1;
	}
	else if (wcscmp(L"AllowVsync", property_name) == 0) {
		*rtn_value = 0;
	}
	else {
		*rtn_value = 0;
	}

	return S_OK;
}

// #3
PCC_EXPORT PCC_GetFloatProperty(wchar_t* property_name, double *rtn_value)
{
	xtra();
	//MessageBoxW(0, property_name, L"PCC_GetFloatProperty", MB_OK);
	if (!rtn_value) {
		return E_POINTER;
	}

	if (wcscmp(L"CPUScore", property_name) == 0) {
		*rtn_value = 6.9;
	}
	else if (wcscmp(L"D3DScore", property_name) == 0) {
		*rtn_value = 7.7;
	}
	else if (wcscmp(L"DiskScore", property_name) == 0) {
		*rtn_value = 5.9;
	}
	else if (wcscmp(L"GraphicsScore", property_name) == 0) {
		*rtn_value = 7.7;
	}
	else if (wcscmp(L"MemoryScore", property_name) == 0) {
		*rtn_value = 7.0;
	}
	else if (wcscmp(L"SystemScore", property_name) == 0) {
		*rtn_value = 5.9;
	}
	else {
		*rtn_value = 0.0;
	}

	return S_OK;
}

// #4
PCC_EXPORT PCC_GetIntegerProperty(wchar_t* property_name, int *rtn_value)
{
	xtra();
	//MessageBoxW(0, property_name, L"PCC_GetIntegerProperty", MB_OK);
	if (!rtn_value) {
		return E_POINTER;
	}

	if (wcscmp(L"VideoDeviceSupportedPixelShaderVersion", property_name) == 0) {
		*rtn_value = 3;
	}
	else if (wcscmp(L"SystemMemory", property_name) == 0) {
		*rtn_value = 0x1FC0;
	}
	else if (wcscmp(L"CpuMaxSpeed", property_name) == 0) {
		*rtn_value = 0xBFB;
	}
	else if (wcscmp(L"CpuLogicalCores", property_name) == 0) {
		*rtn_value = 4;
	}
	else {
		*rtn_value = 0;
	}

	return S_OK;
}

// #5
PCC_EXPORT PCC_GetMessage(int a1, wchar_t *Src) {
	xtra();
	if (!Src) {
		return 0x80070057;
	}
	*(DWORD*)Src = 0;
	return S_OK;
}

// #6
PCC_EXPORT PCC_GetMessageCount(int *a1) {
	xtra();
	*a1 = 0;
	return S_OK;
}

// #7
PCC_EXPORT PCC_GetStringProperty(int a1, DWORD *a2)
{
	xtra();
	if (!a2) {
		return E_POINTER;
	}
	*a2 = 0;
	return S_OK;
}

// #8
PCC_EXPORT PCC_GetVersionProperty(char a1, DWORD *a2)
{
	xtra();
	if (!a2) {
		return E_POINTER;
	}
	*a2 = 0;
	return S_OK;
}

// #9
PCC_EXPORT PCC_Initialize(int a1, void *Src, void *a3, int a4, int a5, int a6)
{
	xtra();
	return S_OK;
}

// #10
PCC_EXPORT PCC_ResetState()
{
	xtra();
	return S_OK;
}

// #11
PCC_EXPORT PCC_SetBooleanProperty(char a1, int a2)
{
	xtra();
	return S_OK;
	return E_FAIL;
}

// #12
PCC_EXPORT PCC_SetFloatProperty(int a1, double a2)
{
	xtra();
	return S_OK;
	return E_FAIL;
}

// #13
PCC_EXPORT PCC_SetIntegerProperty(char a1, int a2)
{
	xtra();
	return S_OK;
	return E_FAIL;
}

// #14
PCC_EXPORT PCC_SetStringProperty(char a1, void *Src)
{
	xtra();
	return S_OK;
	return E_FAIL;
}

// #15
PCC_EXPORT PCC_SetVersionProperty(char a1, int a2, int a3, int a4, int a5)
{
	xtra();
	return S_OK;
	return E_FAIL;
}

// #16
PCC_EXPORT PCC_SetVinceOptions(int a1, int a2, void *Src)
{
	xtra();
	return S_OK;
}

// #17
PCC_EXPORT PCC_ShowConsentUI(HWND hWnd, int a2)
{
	xtra();
	if (!a2) {
		return E_POINTER;
	}
	*(DWORD *)a2 = true ? 1 : 0;//yes selected in dialog box.
	return S_OK;
}

// #18
PCC_EXPORT PCC_ShowMessages(HWND a1, DWORD *a2)
{
	xtra();
	if (a2) {
		*a2 = 1;
	}
	return S_OK;
}

// #19
PCC_EXPORT PCC_ShutdownCheckpoint()
{
	xtra();
	//return S_OK;
	return 1;
}

// #20
PCC_EXPORT PCC_StartupCheckpoint()
{
	xtra();
	return S_OK;
}

// #21
PCC_EXPORT PCC_Uninitialize()
{
	xtra();
	return S_OK;
}
