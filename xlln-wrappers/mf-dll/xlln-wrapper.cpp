#include "../dllmain.hpp"
#include "xlln-wrapper.hpp"
#include "../utils/util-hook.hpp"
#include "../utils/utils.hpp"

#ifdef _DEBUG
#define xtra2() __debugbreak();
#else
#define xtra2() ;
#endif
#define xtra() \
			   xtra2()

typedef void* IMFActivate;
typedef void* IMFAttributes;
typedef void* IMFMediaSession;
typedef void* IMFSourceResolver;
typedef void* IMFTopology;
typedef void* IMFTopologyNode;

enum MF_TOPOLOGY_TYPE {
	MF_TOPOLOGY_OUTPUT_NODE = 0,
	MF_TOPOLOGY_SOURCESTREAM_NODE = 1,
	MF_TOPOLOGY_TRANSFORM_NODE = 2,
	MF_TOPOLOGY_TEE_NODE = 3,
	MF_TOPOLOGY_MAX = 0xFFFFFFFF
};

BOOL InitXllnWrapper()
{

	return TRUE;
}

BOOL UninitXllnWrapper()
{

	return TRUE;
}

MF_EXPORT MFCreateAudioRendererActivate(IMFActivate **ppActivate) {
	xtra();
	if (!ppActivate) {
		return E_POINTER;
	}
	*ppActivate = 0;
	return S_OK;
	return E_OUTOFMEMORY;
}

MF_EXPORT MFCreateMediaSession(IMFAttributes *pConfiguration, IMFMediaSession **ppMediaSession) {
	xtra();
	if (!ppMediaSession) {
		return E_POINTER;
	}
	*ppMediaSession = 0;
	return S_OK;
	return E_OUTOFMEMORY;
	return E_INVALIDARG;
}

MF_EXPORT MFCreateSourceResolver(IMFSourceResolver **ppISourceResolver) {
	//Forwards to MFPlat.dll.
	xtra();
	return S_OK;
}

MF_EXPORT MFCreateTopology(IMFTopology **ppTopo) {
	xtra();
	if (!ppTopo) {
		return E_POINTER;
	}
	*ppTopo = 0;
	return S_OK;
	return E_OUTOFMEMORY;
}

MF_EXPORT MFCreateTopologyNode(MF_TOPOLOGY_TYPE NodeType, IMFTopologyNode **ppNode) {
	xtra();
	if (!ppNode) {
		return E_POINTER;
	}
	*ppNode = 0;
	return S_OK;
	return E_OUTOFMEMORY;
	return E_INVALIDARG;
}

MF_EXPORT MFCreateVideoRendererActivate(HWND hwndVideo, IMFActivate **ppActivate) {
	xtra();
	if (!ppActivate) {
		return E_POINTER;
	}
	*ppActivate = 0;
	return S_OK;
	return E_OUTOFMEMORY;
}

MF_EXPORT MFGetService(IUnknown *punkObject, const GUID *const guidService, const IID *const riid, LPVOID *ppvObject) {
	xtra();
	if (!punkObject) {
		return E_POINTER;
	}
	return S_OK;
}
